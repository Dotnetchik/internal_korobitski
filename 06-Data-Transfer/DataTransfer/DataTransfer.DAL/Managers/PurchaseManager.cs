﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataTransfer.BLL.Interfaces;
using DataTransfer.BLL.Models;
using DataTransfer.DAL.Entities;
using DataTransfer.DAL.Interfaces;

namespace DataTransfer.BLL.Managers
{
    
    public class PurchaseManager: IPurchaseManager
    {
        private readonly IPurchaseDataProvider _purchaseDataProvider;

        public PurchaseManager(IPurchaseDataProvider purchaseDataProvider)
        {
            _purchaseDataProvider = purchaseDataProvider;
        }

        public IEnumerable<Purchase> GetAllPurchases()
            => _purchaseDataProvider.GetAllPurchases().Select(purchase => new Purchase
                {
                    Id=purchase.Id,
                    Name = purchase.Name,
                    CreationDate = purchase.CreationDate,
                    EditingDate = purchase.EditingDate
                });

        public Purchase GetPurchase(Guid id)
        {
            if (id == Guid.Empty) throw new ArgumentNullException(nameof(id));
            var purchase = _purchaseDataProvider.GetPurchase(id);
            return new Purchase
            {
                Id = purchase.Id,
                Name = purchase.Name,
                CreationDate = purchase.CreationDate,
                EditingDate = purchase.EditingDate
            };
        }

        public void AddPurchase(Purchase purchase)
        {
            if (purchase == null) throw new ArgumentNullException(nameof(purchase));
            if (string.IsNullOrEmpty(purchase.Name)) throw new ArgumentNullException(nameof(purchase.Name));
            if (purchase.CreationDate == null) throw new ArgumentNullException(nameof(purchase.CreationDate));
            if (purchase.EditingDate == null) throw new ArgumentNullException(nameof(purchase.EditingDate));

            var addedPurchase = new PurchaseEntity(purchase.Name,purchase.CreationDate,purchase.EditingDate);
            _purchaseDataProvider.AddPurchase(addedPurchase);
        }

        public void EditPurchase(Purchase purchase)
        {
            if (purchase == null) throw new ArgumentNullException(nameof(purchase));
            if (purchase.Id == Guid.Empty) throw new ArgumentNullException(nameof(purchase.Id));
            if (string.IsNullOrEmpty(purchase.Name)) throw new ArgumentNullException(nameof(purchase.Name));
            if (purchase.CreationDate == null) throw new ArgumentNullException(nameof(purchase.CreationDate));
            if (purchase.EditingDate == null) throw new ArgumentNullException(nameof(purchase.EditingDate));

            var editedPurchase= new PurchaseEntity(purchase.Name, purchase.CreationDate, purchase.EditingDate);
            _purchaseDataProvider.EditPurchase(purchase.Id,editedPurchase);
        }

        public void DeletePurchase(Guid id)
        {
            if (id == Guid.Empty) throw new ArgumentNullException(nameof(id));
            _purchaseDataProvider.DeletePurchase(id);
        }
    }
}