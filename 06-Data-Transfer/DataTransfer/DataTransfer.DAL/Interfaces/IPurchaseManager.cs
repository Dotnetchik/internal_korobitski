﻿using System;
using System.Collections.Generic;
using DataTransfer.BLL.Models;

namespace DataTransfer.BLL.Interfaces
{
    public interface IPurchaseManager
    {
        IEnumerable<Purchase> GetAllPurchases();
        Purchase GetPurchase(Guid id);
        void AddPurchase(Purchase purchase);
        void EditPurchase(Purchase purchase);
        void DeletePurchase(Guid id);
    }
}