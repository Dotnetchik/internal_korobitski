﻿using System;

namespace DataTransfer.BLL.Models
{
    public class Purchase
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime CreationDate { get; set; }
        public DateTime EditingDate { get; set; }
    }
}