﻿using System;
using System.Collections.Generic;
using DataTransfer.DAL.Entities;

namespace DataTransfer.DAL.Interfaces
{
    public interface IPurchaseDataProvider
    {
        IEnumerable<PurchaseEntity> GetAllPurchases();
        PurchaseEntity GetPurchase(Guid id);
        void AddPurchase(PurchaseEntity purchase);
        void EditPurchase(Guid id,PurchaseEntity purchase);
        void DeletePurchase(Guid id);
    }
}