﻿using System;

namespace DataTransfer.DAL.Entities
{
    public class PurchaseEntity
    {
        public Guid Id { get; private set; }
        public string Name { get; internal set; }
        public DateTime CreationDate { get; internal set; }
        public DateTime EditingDate { get; internal set; }

        private PurchaseEntity(){}

        public PurchaseEntity(string name, DateTime creationDate, DateTime editingDate)
        {
            Id=Guid.NewGuid();
            Name = name;
            CreationDate = creationDate;
            EditingDate = editingDate;
        }
    }
}