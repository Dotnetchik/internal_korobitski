﻿using System;
using System.Collections.Generic;
using System.Linq;
using DataTransfer.DAL.DataBaseStubs;
using DataTransfer.DAL.Entities;
using DataTransfer.DAL.Interfaces;

namespace DataTransfer.DAL.DataProviders
{
    public class PurchaseDataProvider:IPurchaseDataProvider
    {
        public IEnumerable<PurchaseEntity> GetAllPurchases()
            => PurchaseDataStub.Purchases;

        public PurchaseEntity GetPurchase(Guid id)
        {
            if(id==Guid.Empty)
                throw new ArgumentNullException(nameof(id));

            return PurchaseDataStub.Purchases.FirstOrDefault(x => x.Id == id);
        }

        public void AddPurchase(PurchaseEntity purchase)
        {
            if (purchase == null) throw new ArgumentNullException(nameof(purchase));
            if(string.IsNullOrEmpty(purchase.Name)) throw new ArgumentNullException(nameof(purchase.Name));
            if(purchase.CreationDate==null) throw new ArgumentNullException(nameof(purchase.CreationDate));
            if(purchase.EditingDate==null) throw new ArgumentNullException(nameof(purchase.EditingDate));

            PurchaseDataStub.Purchases.Add(purchase);
        }

        public void EditPurchase(Guid id,PurchaseEntity purchase)
        {
            if (purchase == null) throw new ArgumentNullException(nameof(purchase));
            if(id==Guid.Empty) throw new ArgumentNullException(nameof(id));
            if (string.IsNullOrEmpty(purchase.Name)) throw new ArgumentNullException(nameof(purchase.Name));
            if (purchase.CreationDate == null) throw new ArgumentNullException(nameof(purchase.CreationDate));
            if (purchase.EditingDate == null) throw new ArgumentNullException(nameof(purchase.EditingDate));

            var updatedPurchase = PurchaseDataStub.Purchases.FirstOrDefault(x=>x.Id== id);

            if(updatedPurchase==null)
                throw new NullReferenceException(nameof(purchase));

            updatedPurchase.CreationDate = purchase.CreationDate;
            updatedPurchase.EditingDate = purchase.EditingDate;
            updatedPurchase.Name = purchase.Name;
        }

        public void DeletePurchase(Guid id)
        {
            if (id == Guid.Empty)
                throw new ArgumentNullException(nameof(id));

            var deletedPurchase= PurchaseDataStub.Purchases.FirstOrDefault(x => x.Id == id);
            if (deletedPurchase == null)
                throw new NullReferenceException(nameof(id));
            PurchaseDataStub.Purchases.Remove(deletedPurchase);
        }
    }
}