﻿using System;
using System.Collections.Generic;
using DataTransfer.DAL.Entities;

namespace DataTransfer.DAL.DataBaseStubs
{
    public static class PurchaseDataStub
    {
        public static List<PurchaseEntity> Purchases { get; set; }

        static PurchaseDataStub()
        {
            Initialize();
        }
        
        private static void Initialize()
        {
            Purchases = new List<PurchaseEntity>
            {
                new PurchaseEntity("Bigudi",DateTime.Now,DateTime.Now)
            };
        }
    }
}