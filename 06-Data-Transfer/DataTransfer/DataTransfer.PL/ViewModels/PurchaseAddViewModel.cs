﻿using DataTransfer.BLL.Models;

namespace DataTransfer.PL.ViewModels
{
    public class PurchaseAddViewModel
    {
        public Purchase NewPurchase { get; set; }
    }
}