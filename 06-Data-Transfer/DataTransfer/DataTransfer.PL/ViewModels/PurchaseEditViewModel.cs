﻿using System;
using DataTransfer.BLL.Models;

namespace DataTransfer.PL.ViewModels
{
    public class PurchaseEditViewModel
    {
        public Purchase PurchaseForView { get; set; }
    }
}