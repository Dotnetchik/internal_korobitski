﻿using System.Collections.Generic;
using DataTransfer.BLL.Models;

namespace DataTransfer.PL.ViewModels
{
    public class PurchaseViewModel
    {
        public List<Purchase> Purchases { get; set; }
    }
}