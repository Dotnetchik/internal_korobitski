﻿using System;
using System.Collections.Generic;
using DataTransfer.BLL.Interfaces;
using DataTransfer.BLL.Models;
using DataTransfer.PL.ViewModels;
using Microsoft.AspNetCore.Mvc;

namespace DataTransfer.PL.Controllers
{
    [Route("Purchase")]
    public class PurchaseController : Controller
    {
        private readonly IPurchaseManager _purchaseManager;

        public PurchaseController(IPurchaseManager purchaseManager)
        {
            _purchaseManager = purchaseManager;
        }

        [Route("")]
        [Route("/")]
        [Route("Main")]
        public IActionResult Main()
        {
            var vm = new PurchaseViewModel
            {
                Purchases = new List<Purchase>(_purchaseManager.GetAllPurchases())
            };
            return View(vm);
        }

        [Route("Add")]
        public IActionResult Add()
        {
            var vm = new PurchaseAddViewModel
            {
                NewPurchase = new Purchase
                {
                    CreationDate = DateTime.Now,
                    EditingDate = DateTime.Now
                }
            };
            return View(vm);
        }

        [Route("AcceptAdd")]
        public IActionResult AcceptAdd(PurchaseAddViewModel vm)
        {
            _purchaseManager.AddPurchase(vm.NewPurchase);
            return RedirectToAction("Main");
        }

        [Route("Edit/{id}")]
        public IActionResult Edit(Guid id)
        {
            var selectedPurchase = _purchaseManager.GetPurchase(id);
            var vm = new PurchaseEditViewModel
            {
                PurchaseForView = selectedPurchase
            };
            return View(vm);
        }

        [Route("AcceptEdit")]
        public IActionResult AcceptEdit(PurchaseEditViewModel vm)
        {
            _purchaseManager.EditPurchase(vm.PurchaseForView);
            return RedirectToAction("Main");
        }

        [Route("Back")]
        public IActionResult Back()
        {
            return RedirectToAction("Main");
        }

        [Route("Remove/{id}")]
        public IActionResult Remove(Guid id)
        {
            _purchaseManager.DeletePurchase(id);
            return RedirectToAction("Main");
        }
    }
}