﻿using System;

namespace NinjectIoc.Core.Interfaces
{
    public interface ITimeService
    {
        DateTime GetCurrentDateTimeAccordingUtc(int? offset);
    }
}