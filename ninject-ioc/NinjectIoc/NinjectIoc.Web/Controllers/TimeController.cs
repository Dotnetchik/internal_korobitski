﻿using System.Globalization;
using System.Web.Mvc;
using NinjectIoc.Core.Interfaces;

namespace NinjectIoc.Web.Controllers
{
    [Route("Time")]
    public class TimeController : Controller
    {
        private readonly ITimeService _timeService;

        public TimeController(ITimeService timeService)
        {
            _timeService = timeService;
        }
        [Route("Time/{offset?}")]
        public ActionResult Time(int? offset)
        {
            return Content(_timeService.GetCurrentDateTimeAccordingUtc(offset).ToString(CultureInfo.InvariantCulture));
        }
    }
}