﻿using Ninject.Modules;
using NinjectIoc.Core.Interfaces;
using NinjectIoc.Infrastructure.Services;

namespace NinjectIoc.Web.Utils
{
    public class NinjectRegistrations : NinjectModule
    {
        public override void Load()
        {
            Bind<ITimeService>().To<TimeService>();
        }
    }
}