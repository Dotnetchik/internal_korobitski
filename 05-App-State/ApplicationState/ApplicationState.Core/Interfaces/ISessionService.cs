﻿using System.Collections.Generic;

namespace ApplicationState.Core.Interfaces
{
    public interface ISessionService
    {
        List<KeyValuePair<string,string>> GetAllSession();
        string GetSessionValue(string key);
        void SetSession(string key, string value);
        void DeleteSession(string key);
        bool IsSessionExist(string key);

    }
}