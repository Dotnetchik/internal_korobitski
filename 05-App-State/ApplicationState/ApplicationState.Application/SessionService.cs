﻿using System;
using System.Collections.Generic;
using System.Text;
using ApplicationState.Core.Interfaces;
using Microsoft.AspNetCore.Http;

namespace ApplicationState.Application
{
    public class SessionService:ISessionService
    {
        private HttpContext _context;

        public SessionService(IHttpContextAccessor contextAccessor)
        {
            _context = contextAccessor.HttpContext;
        }
        public List<KeyValuePair<string, string>> GetAllSession()
        {
            List<KeyValuePair<string, string>> allSessions = new List<KeyValuePair<string, string>>();

            var allSessionsId = GetAllSessionsId();
            
            foreach (var sessionId in allSessionsId)
            {
                allSessions.Add(new KeyValuePair<string, string>(sessionId,GetSessionValue(sessionId)));
            }

            return allSessions;
        }

        public string GetSessionValue(string key)
        {
            if (key == null) throw new ArgumentNullException(nameof(key));
            return _context.Session.TryGetValue(key, out var value)
                ? Encoding.ASCII.GetString(value)
                : "";
        }

        public void SetSession(string key, string value)
        {
            if (key == null) throw new ArgumentNullException(nameof(key));
            if (value == null) throw new ArgumentNullException(nameof(value));

            _context.Session.Set(key, Encoding.ASCII.GetBytes(value));
        }

        public void DeleteSession(string key)
        {
            if (key == null) throw new ArgumentNullException(nameof(key));

            _context.Session.Remove(key);
        }

        public bool IsSessionExist(string key)
        {
            if (key == null) throw new ArgumentNullException(nameof(key));
            return _context.Session.TryGetValue(key, out var mock);
        }

        private IEnumerable<string> GetAllSessionsId()
            => _context.Session.Keys;
    }
}