﻿using System;
using ApplicationState.Core.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApplicationState.Web.Controllers
{
    [Route("session")]
    public class SessionController : Controller
    {
        private readonly ISessionService _sessionService;

        public SessionController(ISessionService sessionService)
        {
            _sessionService = sessionService;
        }
        [Route("/")]
        [Route("get")]
        public IActionResult GetAll()
        {
            var sessions = _sessionService.GetAllSession();
            string sessionsForView = "";
            foreach (var session in sessions)
            {
                sessionsForView += $"Id session-{session.Key} value-{session.Value }";
                sessionsForView+= Environment.NewLine;
            }

            return Content(sessionsForView);

        }
        [Route("get/{key:minlength(1)}")]
        public IActionResult Get(string key)
        {
            var sessionValue = _sessionService.GetSessionValue(key);

            return Content(string.IsNullOrWhiteSpace(sessionValue) 
                ? "Session does not exist" : $"Id session-{key} value-{sessionValue}");
        }
        [Route("set/{key:minlength(1)}/{value:minlength(1)}")]
        public IActionResult Set(string key,string value)
        {
            if (_sessionService.IsSessionExist(key))
            {
                return Content($"Session {key} already exist");
            }
            else
            {
                _sessionService.SetSession(key, value);

                return Content($"Session created. Id session-{key} value-{value}");
            }
        }
        [Route("delete/{key:minlength(1)}")]
        public IActionResult Delete(string key)
        {
            
            if (_sessionService.IsSessionExist(key))
            {
                _sessionService.DeleteSession(key);

                return Content($"Session {key} removed.");
            }
            else
            {
                return Content($"Session {key} does not exist");
            }
        }
    }
}