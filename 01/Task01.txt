1.Метод запроса
GET

2.Заголовки запроса
:authority: catalog.onliner.by
:method: GET
:path: /sdapi/catalog.api/search/products?query=%D1%82%D1%80%D1%83%D0%B1%D0%BA
:scheme: https
accept: application/json, text/javascript, */*; q=0.01
accept-encoding: gzip, deflate, br
accept-language: ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7
cookie: _ym_d=1585576848; _ym_uid=1585576848151829852; ouid=snyBDl6B+5GpV2uAAxgvAg==; __gads=ID=97c4ba684c2c59d4:T=1585576850:S=ALNI_MZVbY21_sUdbupFgqgjV2x5EybDjw; catalog_session=1NFrNNyMHcghEIjirYb4yPWg2Pt30MbPQox8jkdx; _rs_uid=306c3909-4e65-4d6a-9917-2fbad9d6448b; _gcl_au=1.1.1344949744.1600963665; _gid=GA1.2.1225057657.1600963665; _ym_visorc_5770561=w; _ym_visorc_1911064=b; _fbp=fb.1.1600963666562.492253454; _ym_isad=2; _rs_n=3; _ga_4Y6NQKE48G=GS1.1.1600963664.2.1.1600964422.60; _ga=GA1.2.1995859368.1585576848
referer: https://catalog.onliner.by/sdapi/catalog/search/iframe
sec-fetch-dest: empty
sec-fetch-mode: cors
sec-fetch-site: same-origin
user-agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/85.0.4183.121 Safari/537.36
x-requested-with: XMLHttpRequest

3.Заголовки ответа
access-control-allow-credentials: true
access-control-allow-origin
cache-control: no-cache, private
content-encoding: gzip
content-type: application/json; charset=utf-8
date: Thu, 24 Sep 2020 16:29:02 GMT
etag: W/"e6cf409baec6e56dbb075c7b3c82810d"
link: <https://catalog.api.onliner.by/search/products?query=%D1%82%D1%80%D1%83%D0%B1%D0%BA&page=2>; rel="next", <https://catalog.api.onliner.by/search/products?query=%D1%82%D1%80%D1%83%D0%B1%D0%BA&page=106>; rel="last"
server: nginx
status: 200
x-content-type-options: nosniff
x-frame-options: SAMEORIGIN
x-xss-protection: 1; mode=block