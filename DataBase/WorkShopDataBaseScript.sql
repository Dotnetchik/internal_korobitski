IF (EXISTS (SELECT name 
FROM master.dbo.sysdatabases 
WHERE name = 'WorkShop'))
DROP DATABASE WorkShop
Go
CREATE DATABASE WorkShop
Go
Use WorkShop
GO
CREATE TABLE [dbo].[AvailableActivity_Order_Position_OrderStatus](
	[ActivityId] [int] NOT NULL,
	[PositionId] [int] NOT NULL,
	[OrderStatusId] [int] NOT NULL,
 CONSTRAINT [PK_AvailableActivity_Order_Position_OrderStatus] PRIMARY KEY CLUSTERED 
(
	[ActivityId] ASC,
	[PositionId] ASC,
	[OrderStatusId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[BreakingCriticalLevelDictionary](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Level] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_BreakingCriticalLevelDictionary] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Breakings](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[EquipmentId] [int] NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[CreationDate] [date] NOT NULL,
	[WorkShiftNumber] [int] NOT NULL,
	[EmployeeId] [int] NOT NULL,
	[CtiticalTypeId] [int] NOT NULL,
	[BreakingName] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Breakings] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employees](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[PositionId] [int] NOT NULL,
	[ShopNumber] [int] NOT NULL,
 CONSTRAINT [PK_Employees] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Equipment](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
	[Cost] [decimal](18, 0) NOT NULL,
	[DeliveryDate] [date] NOT NULL,
	[Provider] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Equipment] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotificationSettings](
	[BreakingLeveld] [int] NOT NULL,
	[EmployeePositionId] [int] NOT NULL,
 CONSTRAINT [PK_NotificationSettings] PRIMARY KEY CLUSTERED 
(
	[BreakingLeveld] ASC,
	[EmployeePositionId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Order_Breaking](
	[OrderId] [int] NOT NULL,
	[BreakingId] [int] NOT NULL,
 CONSTRAINT [PK_Order_Breaking_1] PRIMARY KEY CLUSTERED 
(
	[BreakingId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderActionsDictionary](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Action] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_OrderActionsDictionary] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Orders](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[ParentId] [int] NULL,
	[StatusId] [int] NOT NULL,
	[EmployeeId] [int] NOT NULL,
 CONSTRAINT [PK_Orders] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderStatusesDictionary](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Status] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_OrderStatusesDictionary] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PositionsDictionary](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Position] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_PositionsDictionary] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ShopsDictionary](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_ShopsDictionary] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[AvailableActivity_Order_Position_OrderStatus] ([ActivityId], [PositionId], [OrderStatusId]) VALUES (1, 1, 6)
INSERT [dbo].[AvailableActivity_Order_Position_OrderStatus] ([ActivityId], [PositionId], [OrderStatusId]) VALUES (2, 2, 2)
INSERT [dbo].[AvailableActivity_Order_Position_OrderStatus] ([ActivityId], [PositionId], [OrderStatusId]) VALUES (3, 1, 4)
INSERT [dbo].[AvailableActivity_Order_Position_OrderStatus] ([ActivityId], [PositionId], [OrderStatusId]) VALUES (3, 2, 4)
INSERT [dbo].[AvailableActivity_Order_Position_OrderStatus] ([ActivityId], [PositionId], [OrderStatusId]) VALUES (3, 3, 4)
INSERT [dbo].[AvailableActivity_Order_Position_OrderStatus] ([ActivityId], [PositionId], [OrderStatusId]) VALUES (4, 2, 2)
GO
SET IDENTITY_INSERT [dbo].[BreakingCriticalLevelDictionary] ON 

INSERT [dbo].[BreakingCriticalLevelDictionary] ([Id], [Level]) VALUES (1, N'Minor')
INSERT [dbo].[BreakingCriticalLevelDictionary] ([Id], [Level]) VALUES (2, N'Major')
INSERT [dbo].[BreakingCriticalLevelDictionary] ([Id], [Level]) VALUES (3, N'Critical')
INSERT [dbo].[BreakingCriticalLevelDictionary] ([Id], [Level]) VALUES (4, N'Blocker')
SET IDENTITY_INSERT [dbo].[BreakingCriticalLevelDictionary] OFF
GO
SET IDENTITY_INSERT [dbo].[Breakings] ON 

INSERT [dbo].[Breakings] ([Id], [EquipmentId], [Description], [CreationDate], [WorkShiftNumber], [EmployeeId], [CtiticalTypeId], [BreakingName]) VALUES (1, 2, N'Открутился болтик', CAST(N'2020-11-12' AS Date), 32, 1, 1, N'Станок')
INSERT [dbo].[Breakings] ([Id], [EquipmentId], [Description], [CreationDate], [WorkShiftNumber], [EmployeeId], [CtiticalTypeId], [BreakingName]) VALUES (2, 3, N'Попер черный дым', CAST(N'2020-11-11' AS Date), 31, 1, 4, N'Главный станок')
SET IDENTITY_INSERT [dbo].[Breakings] OFF
GO
SET IDENTITY_INSERT [dbo].[Employees] ON 

INSERT [dbo].[Employees] ([Id], [Name], [PositionId], [ShopNumber]) VALUES (1, N'Михал Палыч', 1, 1)
INSERT [dbo].[Employees] ([Id], [Name], [PositionId], [ShopNumber]) VALUES (2, N'Валентин Витольдович', 2, 1)
SET IDENTITY_INSERT [dbo].[Employees] OFF
GO
SET IDENTITY_INSERT [dbo].[Equipment] ON 

INSERT [dbo].[Equipment] ([Id], [Name], [Cost], [DeliveryDate], [Provider]) VALUES (2, N'Рубанок ручной', CAST(50 AS Decimal(18, 0)), CAST(N'2020-01-01' AS Date), N'Витя и партнеры')
INSERT [dbo].[Equipment] ([Id], [Name], [Cost], [DeliveryDate], [Provider]) VALUES (3, N'Токарный станок,импортный', CAST(5000000 AS Decimal(18, 0)), CAST(N'2020-01-01' AS Date), N'Germany Reach Company')
SET IDENTITY_INSERT [dbo].[Equipment] OFF
GO
INSERT [dbo].[NotificationSettings] ([BreakingLeveld], [EmployeePositionId]) VALUES (4, 2)
GO
INSERT [dbo].[Order_Breaking] ([OrderId], [BreakingId]) VALUES (1, 1)
INSERT [dbo].[Order_Breaking] ([OrderId], [BreakingId]) VALUES (2, 2)
GO
SET IDENTITY_INSERT [dbo].[OrderActionsDictionary] ON 

INSERT [dbo].[OrderActionsDictionary] ([Id], [Action]) VALUES (1, N'Сформировать')
INSERT [dbo].[OrderActionsDictionary] ([Id], [Action]) VALUES (2, N'Отклонить')
INSERT [dbo].[OrderActionsDictionary] ([Id], [Action]) VALUES (3, N'Доработать')
INSERT [dbo].[OrderActionsDictionary] ([Id], [Action]) VALUES (4, N'Отправить на доработку')
INSERT [dbo].[OrderActionsDictionary] ([Id], [Action]) VALUES (5, N'Выполнить')
INSERT [dbo].[OrderActionsDictionary] ([Id], [Action]) VALUES (6, N'Взять в работу')
SET IDENTITY_INSERT [dbo].[OrderActionsDictionary] OFF
GO
SET IDENTITY_INSERT [dbo].[Orders] ON 

INSERT [dbo].[Orders] ([Id], [ParentId], [StatusId], [EmployeeId]) VALUES (1, NULL, 1, 1)
INSERT [dbo].[Orders] ([Id], [ParentId], [StatusId], [EmployeeId]) VALUES (2, 1, 1, 1)
SET IDENTITY_INSERT [dbo].[Orders] OFF
GO
SET IDENTITY_INSERT [dbo].[OrderStatusesDictionary] ON 

INSERT [dbo].[OrderStatusesDictionary] ([Id], [Status]) VALUES (1, N'Создана')
INSERT [dbo].[OrderStatusesDictionary] ([Id], [Status]) VALUES (2, N'На соглосовании')
INSERT [dbo].[OrderStatusesDictionary] ([Id], [Status]) VALUES (3, N'Отклонена')
INSERT [dbo].[OrderStatusesDictionary] ([Id], [Status]) VALUES (4, N'Требуется дороботка')
INSERT [dbo].[OrderStatusesDictionary] ([Id], [Status]) VALUES (5, N'Выполнена')
INSERT [dbo].[OrderStatusesDictionary] ([Id], [Status]) VALUES (6, N'Может быть создана')
SET IDENTITY_INSERT [dbo].[OrderStatusesDictionary] OFF
GO
SET IDENTITY_INSERT [dbo].[PositionsDictionary] ON 

INSERT [dbo].[PositionsDictionary] ([Id], [Position]) VALUES (1, N'Слесарь')
INSERT [dbo].[PositionsDictionary] ([Id], [Position]) VALUES (2, N'Начальник')
INSERT [dbo].[PositionsDictionary] ([Id], [Position]) VALUES (3, N'Работник')
SET IDENTITY_INSERT [dbo].[PositionsDictionary] OFF
GO
SET IDENTITY_INSERT [dbo].[ShopsDictionary] ON 

INSERT [dbo].[ShopsDictionary] ([Id], [Name]) VALUES (1, N'Трудяги')
INSERT [dbo].[ShopsDictionary] ([Id], [Name]) VALUES (2, N'Второй Состав')
SET IDENTITY_INSERT [dbo].[ShopsDictionary] OFF
GO
ALTER TABLE [dbo].[AvailableActivity_Order_Position_OrderStatus]  WITH CHECK ADD  CONSTRAINT [FK_AvailableActivity_Order_Position_OrderActionsDictionary] FOREIGN KEY([ActivityId])
REFERENCES [dbo].[OrderActionsDictionary] ([Id])
GO
ALTER TABLE [dbo].[AvailableActivity_Order_Position_OrderStatus] CHECK CONSTRAINT [FK_AvailableActivity_Order_Position_OrderActionsDictionary]
GO
ALTER TABLE [dbo].[AvailableActivity_Order_Position_OrderStatus]  WITH CHECK ADD  CONSTRAINT [FK_AvailableActivity_Order_Position_OrderStatus_OrderStatusesDictionary] FOREIGN KEY([OrderStatusId])
REFERENCES [dbo].[OrderStatusesDictionary] ([Id])
GO
ALTER TABLE [dbo].[AvailableActivity_Order_Position_OrderStatus] CHECK CONSTRAINT [FK_AvailableActivity_Order_Position_OrderStatus_OrderStatusesDictionary]
GO
ALTER TABLE [dbo].[AvailableActivity_Order_Position_OrderStatus]  WITH CHECK ADD  CONSTRAINT [FK_AvailableActivity_Order_Position_PositionsDictionary] FOREIGN KEY([PositionId])
REFERENCES [dbo].[PositionsDictionary] ([Id])
GO
ALTER TABLE [dbo].[AvailableActivity_Order_Position_OrderStatus] CHECK CONSTRAINT [FK_AvailableActivity_Order_Position_PositionsDictionary]
GO
ALTER TABLE [dbo].[Breakings]  WITH CHECK ADD  CONSTRAINT [FK_Breakings_BreakingCriticalLevelDictionary] FOREIGN KEY([CtiticalTypeId])
REFERENCES [dbo].[BreakingCriticalLevelDictionary] ([Id])
GO
ALTER TABLE [dbo].[Breakings] CHECK CONSTRAINT [FK_Breakings_BreakingCriticalLevelDictionary]
GO
ALTER TABLE [dbo].[Breakings]  WITH CHECK ADD  CONSTRAINT [FK_Breakings_Equipment] FOREIGN KEY([EquipmentId])
REFERENCES [dbo].[Equipment] ([Id])
GO
ALTER TABLE [dbo].[Breakings] CHECK CONSTRAINT [FK_Breakings_Equipment]
GO
ALTER TABLE [dbo].[Employees]  WITH CHECK ADD  CONSTRAINT [FK_Employees_PositionsDictionary] FOREIGN KEY([PositionId])
REFERENCES [dbo].[PositionsDictionary] ([Id])
GO
ALTER TABLE [dbo].[Employees] CHECK CONSTRAINT [FK_Employees_PositionsDictionary]
GO
ALTER TABLE [dbo].[Employees]  WITH CHECK ADD  CONSTRAINT [FK_Employees_ShopsDictionary] FOREIGN KEY([ShopNumber])
REFERENCES [dbo].[ShopsDictionary] ([Id])
GO
ALTER TABLE [dbo].[Employees] CHECK CONSTRAINT [FK_Employees_ShopsDictionary]
GO
ALTER TABLE [dbo].[NotificationSettings]  WITH CHECK ADD  CONSTRAINT [FK_NotificationSettings_BreakingCriticalLevelDictionary] FOREIGN KEY([BreakingLeveld])
REFERENCES [dbo].[BreakingCriticalLevelDictionary] ([Id])
GO
ALTER TABLE [dbo].[NotificationSettings] CHECK CONSTRAINT [FK_NotificationSettings_BreakingCriticalLevelDictionary]
GO
ALTER TABLE [dbo].[NotificationSettings]  WITH CHECK ADD  CONSTRAINT [FK_NotificationSettings_PositionsDictionary] FOREIGN KEY([EmployeePositionId])
REFERENCES [dbo].[PositionsDictionary] ([Id])
GO
ALTER TABLE [dbo].[NotificationSettings] CHECK CONSTRAINT [FK_NotificationSettings_PositionsDictionary]
GO
ALTER TABLE [dbo].[Order_Breaking]  WITH CHECK ADD  CONSTRAINT [FK_Order_Breaking_Breakings] FOREIGN KEY([BreakingId])
REFERENCES [dbo].[Breakings] ([Id])
GO
ALTER TABLE [dbo].[Order_Breaking] CHECK CONSTRAINT [FK_Order_Breaking_Breakings]
GO
ALTER TABLE [dbo].[Order_Breaking]  WITH CHECK ADD  CONSTRAINT [FK_Order_Breaking_Orders] FOREIGN KEY([OrderId])
REFERENCES [dbo].[Orders] ([Id])
GO
ALTER TABLE [dbo].[Order_Breaking] CHECK CONSTRAINT [FK_Order_Breaking_Orders]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_Employees] FOREIGN KEY([EmployeeId])
REFERENCES [dbo].[Employees] ([Id])
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_Employees]
GO
ALTER TABLE [dbo].[Orders]  WITH CHECK ADD  CONSTRAINT [FK_Orders_OrderStatusesDictionary] FOREIGN KEY([StatusId])
REFERENCES [dbo].[OrderStatusesDictionary] ([Id])
GO
ALTER TABLE [dbo].[Orders] CHECK CONSTRAINT [FK_Orders_OrderStatusesDictionary]
GO
USE [master]
GO
ALTER DATABASE [WorkShop] SET  READ_WRITE 
