﻿using CleanArchitecture.Core.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace CleanArchitecture.WEB.Controllers
{
    public class CountryController : Controller
    {
        private readonly ICountryManager _countryManager;

        public CountryController(ICountryManager countryManager)
        {
            _countryManager = countryManager;
        }
        // GET
        public IActionResult Index()
        {
            var country = _countryManager.GetCountry();
            var countryForView = $"{country.Name}PL";
            return Content(countryForView);
        }
    }
}