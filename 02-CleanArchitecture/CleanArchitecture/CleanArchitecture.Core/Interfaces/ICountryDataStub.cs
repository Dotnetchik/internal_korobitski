﻿using CleanArchitecture.Core.Enteties;

namespace CleanArchitecture.Core.Interfaces
{
    public interface ICountryDataStub
    {
        CountryEntity GetCountryEntity();
    }
}