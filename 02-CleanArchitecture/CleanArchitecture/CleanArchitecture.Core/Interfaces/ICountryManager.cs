﻿using CleanArchitecture.Core.Models;

namespace CleanArchitecture.Core.Interfaces
{
    public interface ICountryManager
    {
        Country GetCountry();
    }
}