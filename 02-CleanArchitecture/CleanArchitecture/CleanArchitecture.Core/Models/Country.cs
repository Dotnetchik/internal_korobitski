﻿namespace CleanArchitecture.Core.Models
{
    public class Country
    {
        public string Name { get; set; }
    }
}