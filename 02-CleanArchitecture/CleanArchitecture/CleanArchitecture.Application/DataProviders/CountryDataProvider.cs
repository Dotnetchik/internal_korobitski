﻿using CleanArchitecture.Core.Enteties;
using CleanArchitecture.Core.Interfaces;

namespace CleanArchitecture.Infrastructure.DataProviders
{
    public class CountryDataProvider:ICountryDataStub
    {
        public CountryEntity GetCountryEntity()
            => new CountryEntity {Name = "Pakistan"};
    }
}