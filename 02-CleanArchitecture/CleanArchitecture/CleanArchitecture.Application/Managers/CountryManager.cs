﻿using CleanArchitecture.Core.Interfaces;
using CleanArchitecture.Core.Models;

namespace CleanArchitecture.Infrastructure.Managers
{
    public class CountryManager:ICountryManager
    {
        private readonly ICountryDataStub _countryDataStub;

        public CountryManager(ICountryDataStub countryDataStub)
        {
            _countryDataStub = countryDataStub;
        }
        public Country GetCountry()
        {
            var countryEntity = _countryDataStub.GetCountryEntity();
            return new Country{Name = $"{countryEntity.Name}BL"};
        }
    }
}