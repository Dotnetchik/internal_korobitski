﻿using Architecture.BL.Interfaces;
using Architecture.BL.Model;
using Architecture.DAL.Interfaces;

namespace Architecture.BL.Managers
{
    public class CountryManager:ICountryManager
    {
        private readonly ICountryDataStub _countryDataStub;

        public CountryManager(ICountryDataStub countryDataStub)
        {
            _countryDataStub = countryDataStub;
        }
        public Country GeCountry()
        {
            var countryEntity = _countryDataStub.GetCountryEntity();
            return new Country
            {
                Name = $"{countryEntity.Name}BL"
            };
        }
    }
}