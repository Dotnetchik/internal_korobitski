﻿using Architecture.BL.Model;

namespace Architecture.BL.Interfaces
{
    public interface ICountryManager
    {
        Country GeCountry();
    }
}