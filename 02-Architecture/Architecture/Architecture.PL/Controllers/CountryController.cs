﻿using Architecture.BL.Interfaces;
using Architecture.PL.Models;
using Microsoft.AspNetCore.Mvc;

namespace Architecture.PL.Controllers
{
    public class CountryController : Controller
    {
        private readonly ICountryManager _countryManager;

        public CountryController(ICountryManager countryManager)
        {
            _countryManager = countryManager;
        }
        // GET
        public IActionResult Index()
        {
            var country = _countryManager.GeCountry();
            var countryForView = new CountryModel {Name = $"{country.Name}PL"};
            return Content(countryForView.Name);
        }
    }
}