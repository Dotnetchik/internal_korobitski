﻿using Architecture.DAL.Entities;
using Architecture.DAL.Interfaces;

namespace Architecture.DAL.DataProvider
{
    public class CountryDataStub: ICountryDataStub
    {
        public CountryEntity GetCountryEntity()
            => new CountryEntity{Name = "Pakistan"};
    }
}