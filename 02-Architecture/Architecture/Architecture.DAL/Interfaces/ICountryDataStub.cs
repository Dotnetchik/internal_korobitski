﻿using Architecture.DAL.Entities;

namespace Architecture.DAL.Interfaces
{
    public interface ICountryDataStub
    {
        CountryEntity GetCountryEntity();
    }
}