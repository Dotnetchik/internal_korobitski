﻿using System;

namespace AutofacIoc.Core.Interfaces
{
    public interface ITimeService
    {
        DateTime GetCurrentDateTimeAccordingUtc(int offset);
    }
}