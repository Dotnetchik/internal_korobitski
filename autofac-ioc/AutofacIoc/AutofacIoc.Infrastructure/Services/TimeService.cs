﻿using System;
using AutofacIoc.Core.Interfaces;

namespace AutofacIoc.Infrastructure.Services
{
    public class TimeService:ITimeService
    {
        public DateTime GetCurrentDateTimeAccordingUtc(int offset)
        {
            if (offset <= -12 || offset>=12) throw new ArgumentOutOfRangeException(nameof(offset));
            return DateTime.UtcNow.AddHours(offset);
        }
    }
}