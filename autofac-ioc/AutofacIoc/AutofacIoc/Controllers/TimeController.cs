﻿using System.Globalization;
using AutofacIoc.Core.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace AutofacIoc.Web.Controllers
{
    public class TimeController : Controller
    {
        private readonly ITimeService _timeService;

        public TimeController(ITimeService timeService)
        {
            _timeService = timeService;
        }
        public IActionResult Time(int offset)
        {
            return Content(_timeService.GetCurrentDateTimeAccordingUtc(offset).ToString(CultureInfo.InvariantCulture));
        }
    }
}