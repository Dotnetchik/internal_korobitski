﻿using System;
using System.Text;
using Filters.Core.Interfaces;
using Microsoft.AspNetCore.Http;

namespace Filters.Application.Services
{
    public class SessionService:ISessionService
    {
        private readonly HttpContext _context;
        public SessionService(IHttpContextAccessor contextAccessor)
        {
            _context = contextAccessor.HttpContext;
        }
        public void AddSession(string key, string value)
        {
            if (key == null) throw new ArgumentNullException(nameof(key));
            if (value == null) throw new ArgumentNullException(nameof(value));

            _context.Session.Set(key, Encoding.ASCII.GetBytes(value));
        }

        public void RemoveSession(string key)
        {
            if (key == null) throw new ArgumentNullException(nameof(key));

            _context.Session.Remove(key);
        }

        public bool IsSessionExist(string key)
        {
            if (key == null) throw new ArgumentNullException(nameof(key));
            return _context.Session.TryGetValue(key, out var mock);
        }

    }
}