﻿using System;
using Filters.Core.Interfaces;
using Filters.Core.Models;

namespace Filters.Application.Services
{
    public class InputParametersService:IInputParametersService
    {
        public bool IsValidLength(string value, int length)
            => value.Length <= length;

        public string GetValidLength(string value, int length)
            => value?.Length > length ? value.Substring(0, length - 1):value;

        public string ToUppercase(string value)
            => value?.ToUpper();
    }
}