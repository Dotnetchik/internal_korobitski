﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Filters.Core.Interfaces;
using Filters.Core.Models;
using Filters.Web.Filters;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Filters.Web.Models;
using Microsoft.Extensions.Options;

namespace Filters.Web.Controllers
{
    [Route("")]
    [Route("Home")]
    public class HomeController : Controller
    {
        private readonly ISessionService _sessionService;
        private readonly ILogger<HomeController> _logger;
        private readonly SessionSettingsModel _sessionSettings;

        public HomeController(ISessionService sessionService, IOptions<SessionSettingsModel> sessionOptions)
        {
            _sessionService = sessionService;
            _sessionSettings = sessionOptions.Value;
        }

        [TypeFilter(typeof(SessionCheckerWithRedirectFilter))]
        [TypeFilter(typeof(RequestParametersFilter))]
        [TypeFilter(typeof(ResponseFormattingFilter))]
        [Route("Index/{str}")]
        public IActionResult Index(string str)
        {
            return Content(str);
        }
        [TypeFilter(typeof(SessionCheckFilter), Arguments = new Object[] { false })]
        [Route("RegisterSession")]
        [Route("/")]
        public IActionResult RegisterSession()
        {
            _sessionService.AddSession(_sessionSettings.SessionId, "ok1");

            return Content("Сессия создана");
        }
        
        [TypeFilter(typeof(SessionCheckFilter), Arguments = new Object[] { true })]
        [Route("RemoveSession")]
        public IActionResult RemoveSession()
        {
            _sessionService.RemoveSession(_sessionSettings.SessionId);

            return Content("Сессия удалена");
        }

    }
}
