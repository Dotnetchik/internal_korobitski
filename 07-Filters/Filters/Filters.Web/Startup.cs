using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Filters.Application.Services;
using Filters.Core.Interfaces;
using Filters.Core.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Filters.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .Build();
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();

            services.Configure<FiltersActivityStatusModel>(Configuration.GetSection(
                FiltersActivityStatusModel.FiltersActivityStatus));
            services.Configure<SessionSettingsModel>(Configuration.GetSection(
                SessionSettingsModel.Sessions));
            services.Configure<InputParametersValidationModel>(Configuration.GetSection(
                InputParametersValidationModel.InputParametersValidation));

            services.AddDistributedMemoryCache();

            services.AddSession();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddScoped<ISessionService, SessionService>();
            services.AddScoped<IInputParametersService, InputParametersService>();
            services.AddScoped<IStreamService, StreamService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSession();
            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "Home/{action=RegisterSession}");
            });
        }
    }
}
