﻿using System;
using Filters.Core.Interfaces;
using Filters.Core.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Options;

namespace Filters.Web.Filters
{
    public class SessionCheckerWithRedirectFilter : Attribute, IAuthorizationFilter
    {
        private readonly ISessionService _sessionService;
        private readonly FiltersActivityStatusModel _filtersActivityStatus;
        private readonly SessionSettingsModel _sessionSettings;
        private const string SessionNotExistMessage = "Сессия не существует";
        private const string SessionExistMessage = "Сессия уже существует";

        public SessionCheckerWithRedirectFilter(IOptions<FiltersActivityStatusModel> filterOptions,
            IOptions<SessionSettingsModel> sessionOptions, ISessionService sessionService)
        {
            _sessionService = sessionService;
            _filtersActivityStatus = filterOptions.Value;
            _sessionSettings = sessionOptions.Value;
        }
        public void OnAuthorization(AuthorizationFilterContext context)
        {
            if (!_filtersActivityStatus.IsSessionCheckFilterActive) return;

            if (!_sessionService.IsSessionExist(_sessionSettings.SessionId))
                context.Result=new RedirectToActionResult("RegisterSession", "Home",context);
                
        }
    }
}