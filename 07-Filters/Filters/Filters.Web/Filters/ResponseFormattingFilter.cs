﻿using System;
using Filters.Core.Interfaces;
using Filters.Core.Models;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Options;

namespace Filters.Web.Filters
{
    public class ResponseFormattingFilter:Attribute,IResultFilter
    {
        private readonly IStreamService _streamService;
        private readonly FiltersActivityStatusModel _filtersActivityStatus;

        public ResponseFormattingFilter(IOptions<FiltersActivityStatusModel> filterOptions,IStreamService streamService)
        {
            _streamService = streamService;
            _filtersActivityStatus = filterOptions.Value;
        }
        public void OnResultExecuted(ResultExecutedContext context)
        {
            if (_filtersActivityStatus.IsResponseFormattingFilterActive)
            {
                context.HttpContext.Response.Body = _streamService.ToUppercase(context.HttpContext.Response.Body);
            }
            
        }

        public void OnResultExecuting(ResultExecutingContext context)
        {
            }
    }
}