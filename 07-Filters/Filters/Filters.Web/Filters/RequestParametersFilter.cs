﻿using System;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Filters.Core.Interfaces;
using Filters.Core.Models;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Options;

namespace Filters.Web.Filters
{
    public class RequestParametersFilter:Attribute,IAsyncActionFilter
    {
        private readonly IInputParametersService _parametersService;
        private readonly IStreamService _streamService;
        private readonly FiltersActivityStatusModel _filtersActivityStatus;
        private readonly InputParametersValidationModel _inputParametersValidationRule;
        public RequestParametersFilter(IOptions<FiltersActivityStatusModel> filterOptions,
            IOptions<InputParametersValidationModel> inputParametersOptions,
            IInputParametersService parametersService,IStreamService streamService)
        {
            _parametersService = parametersService;
            _streamService = streamService;
            _filtersActivityStatus = filterOptions.Value;
            _inputParametersValidationRule = inputParametersOptions.Value;
        }

        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            if (_filtersActivityStatus.IsRequestParametersFilterActive)
            {
                var inputParameter = context.ActionArguments["str"].ToString();
                if (!_parametersService.IsValidLength(inputParameter, _inputParametersValidationRule.MaxLength))
                    context.ActionArguments["str"] = _parametersService.GetValidLength(inputParameter, _inputParametersValidationRule.MaxLength);
            }
            
            await next();

            if (_filtersActivityStatus.IsResponseAddDateFilterActive)
            {
                context.HttpContext.Response.Body = _streamService.AddText(context.HttpContext.Response.Body,DateTime.Now.ToString());
            }
            
        }
        
    }
}