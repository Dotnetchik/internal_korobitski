﻿using System;
using System.Threading.Tasks;
using Filters.Core.Interfaces;
using Filters.Core.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Options;

namespace Filters.Web.Filters
{
    public class SessionCheckFilter : Attribute, IAuthorizationFilter
    {
        private readonly bool _isSessionMustBeExist;
        private readonly ISessionService _sessionService;
        private readonly FiltersActivityStatusModel _filtersActivityStatus;
        private readonly SessionSettingsModel _sessionSettings;
        private const string SessionNotExistMessage="Сессия не существует";
        private const string SessionExistMessage="Сессия уже существует";

        public SessionCheckFilter(bool isSessionMustBeExist, IOptions<FiltersActivityStatusModel> filterOptions,
            IOptions<SessionSettingsModel> sessionOptions,ISessionService sessionService)
        {
            _isSessionMustBeExist = isSessionMustBeExist;
            _sessionService = sessionService;
            _filtersActivityStatus = filterOptions.Value;
            _sessionSettings = sessionOptions.Value;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            if (!_filtersActivityStatus.IsSessionCheckFilterActive) return;
            if (_isSessionMustBeExist)
            {
                if (!_sessionService.IsSessionExist(_sessionSettings.SessionId))
                    context.Result=new ContentResult
                    {
                        Content = SessionNotExistMessage
                    };
            }
            else
            {
                if (_sessionService.IsSessionExist(_sessionSettings.SessionId))
                    context.Result = new ContentResult
                    {
                        Content = SessionExistMessage
                    };
            }
        }
    }
}