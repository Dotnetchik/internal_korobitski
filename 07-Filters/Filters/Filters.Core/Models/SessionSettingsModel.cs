﻿namespace Filters.Core.Models
{
    public class SessionSettingsModel
    {
        public const string Sessions = "Sessions";
        public string SessionId { get; set; }
    }
}