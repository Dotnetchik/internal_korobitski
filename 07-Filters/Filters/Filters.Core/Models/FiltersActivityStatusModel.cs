﻿namespace Filters.Core.Models
{
    public class FiltersActivityStatusModel
    {
        public const string FiltersActivityStatus = "FiltersActivityStatus";
        public bool IsSessionCheckFilterActive { get; set; }
        public bool IsRequestParametersFilterActive { get; set; }
        public bool IsResponseAddDateFilterActive { get; set; }
        public bool IsResponseFormattingFilterActive { get; set; }
    }
}