﻿namespace Filters.Core.Interfaces
{
    public interface ISessionService
    {
        bool IsSessionExist(string key);
        void AddSession(string key, string value);
        void RemoveSession(string key);
    }
}