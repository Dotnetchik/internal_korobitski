﻿using System.IO;

namespace Filters.Core.Interfaces
{
    public interface IStreamService
    {
        Stream ToUppercase(Stream stream);
        Stream AddText(Stream stream, string text);
    }
}