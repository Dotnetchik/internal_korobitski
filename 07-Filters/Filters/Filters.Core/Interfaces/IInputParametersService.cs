﻿namespace Filters.Core.Interfaces
{
    public interface IInputParametersService
    {
        bool IsValidLength(string value, int length);
        string GetValidLength(string value, int length);
        string ToUppercase(string value);
    }
}