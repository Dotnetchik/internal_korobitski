﻿using Microsoft.AspNetCore.Mvc;

namespace Routing.Web.Controllers
{
    public class BookController : Controller
    {
        // GET
        public IActionResult Index(int bookId)
        {
            return Content(bookId.ToString());
        }
    }
}