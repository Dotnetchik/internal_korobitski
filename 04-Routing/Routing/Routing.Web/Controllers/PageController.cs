﻿using Microsoft.AspNetCore.Mvc;

namespace Routing.Web.Controllers
{
    
    [Route("page")]
    public class PageController : Controller
    {
        [Route("{AnyMethods}/{pageName:maxlength(5)}")]
        public IActionResult Get(string pageName)
        {
            return Content(pageName);
        }
    }
}