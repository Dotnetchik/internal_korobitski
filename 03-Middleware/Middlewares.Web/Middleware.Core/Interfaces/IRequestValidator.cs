﻿namespace Middleware.Core.Interfaces
{
    public interface IRequestValidator
    {
        bool IsQueryLengthValid(string query,int maxLength);
        bool IsQueryContainsUnavailableSign(string query,char[] pattern);
    }
}