﻿namespace Middleware.Core.ValidationModels
{
    public class RequestValidationModel
    {
        public const string QueryRules = "QueryRules";
        public int MaxLength { get; set; }
        public char[] UnAvailableSign { get; set; }
    }
}