﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Middleware.Core.Interfaces;
using Middleware.Core.ValidationModels;

namespace Middlewares.Web.Middlewares
{
    public class RequestAvailableSignMiddleware
    {
        private readonly RequestDelegate _next;
        private RequestValidationModel RequestValidationRules { get; }
        public RequestAvailableSignMiddleware(RequestDelegate next, IOptions<RequestValidationModel> options)
        {
            _next = next;
            RequestValidationRules = options.Value;
        }
        public async Task InvokeAsync(HttpContext context, IRequestValidator validator)
        {
            if (validator.IsQueryContainsUnavailableSign(context.Request.QueryString.Value,RequestValidationRules.UnAvailableSign))
            {
                context.Response.StatusCode = 400;
            }
            else
            {
                await _next(context);
            }
        }
    }
}