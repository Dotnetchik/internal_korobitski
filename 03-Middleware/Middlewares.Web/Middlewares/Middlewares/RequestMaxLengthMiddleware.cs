﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Middleware.Core.Interfaces;
using Middleware.Core.ValidationModels;

namespace Middlewares.Web.Middlewares
{
    public class RequestMaxLengthMiddleware
    {
        private readonly RequestDelegate _next;
        private RequestValidationModel RequestValidationRules { get; }
        public RequestMaxLengthMiddleware(RequestDelegate next, IOptions<RequestValidationModel> options)
        {
            _next = next;
            RequestValidationRules = options.Value;
        }
        public async Task InvokeAsync(HttpContext context,IRequestValidator validator)
        {
            if (validator.IsQueryLengthValid(context.Request.QueryString.Value, RequestValidationRules.MaxLength))
            {
                await _next(context);
            }
            else
            {
                context.Response.StatusCode = 400;
            }
        }
    }
}