﻿using Microsoft.AspNetCore.Builder;
using Middlewares.Web.Middlewares;

namespace Middlewares.Web.Extensions
{
    public static class ApplicationBuilderExtensions
    {
        public static IApplicationBuilder UseRequestLengthCheck(this IApplicationBuilder app)
            => app.UseMiddleware<RequestMaxLengthMiddleware>();
        public static IApplicationBuilder UseRequestSignCheck(this IApplicationBuilder app)
            => app.UseMiddleware<RequestAvailableSignMiddleware>();
    }
}