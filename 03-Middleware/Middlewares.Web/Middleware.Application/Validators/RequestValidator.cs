﻿using Middleware.Core.Interfaces;
using Middleware.Core.ValidationModels;

namespace Middleware.Application.Validators
{
    public class RequestValidator:IRequestValidator
    {
        public bool IsQueryLengthValid(string query, int maxLength)
            => query.Length <= maxLength;

        public bool IsQueryContainsUnavailableSign(string query, char[] pattern)
            => query.IndexOfAny(pattern) > 0;
    }
}